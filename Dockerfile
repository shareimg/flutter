# Base image
# ---------------------------------------------------------------------- #
FROM arm64v8/ubuntu:20.04


# install essential tools
RUN apt-get update && \
    apt-get dist-upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends git wget wget2 zip unzip xdg-utils && \
    apt-get install -y build-essential vim

RUN apt-get install -y bash curl file xz-utils libglu1-mesa && \
    git clone https://github.com/flutter/flutter.git -b master
	
# install flutter
ENV PATH="$PATH:/flutter/bin"

RUN flutter config --no-analytics && \
    flutter precache

RUN flutter channel stable && flutter upgrade

# install Android SDK
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y android-sdk && \
    rm -f /usr/lib/android-sdk/build-tools/27.0.1

# install Android SDK cmdline tools
ARG ANDROID_SDK_VERSION=10406996
ENV ANDROID_SDK_ROOT /usr/lib/android-sdk
RUN wget2 -q https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_VERSION}_latest.zip && \
    unzip commandlinetools*linux*${ANDROID_SDK_VERSION}*.zip && \
    mv cmdline-tools tools && \
    mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools && \
    mv tools ${ANDROID_SDK_ROOT}/cmdline-tools/ && \
    (cd /usr/lib/android-sdk/cmdline-tools; ln -s tools latest) && \
    rm -f commandlinetools*linux*.zip


# download and install Gradle
ARG GRADLE_VERSION=8.2.1
ARG GRADLE_DIST=bin
RUN wget2 -q https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-${GRADLE_DIST}.zip && \
    rm -fr /usr/share/gradle && \
    unzip gradle*.zip -d /usr/share && \
    rm gradle*.zip && \
    mv /usr/share/gradle-${GRADLE_VERSION} /usr/share/gradle

# download and install Kotlin compiler
ARG KOTLIN_VERSION=1.9.0
RUN wget2 -q https://github.com/JetBrains/kotlin/releases/download/v${KOTLIN_VERSION}/kotlin-compiler-${KOTLIN_VERSION}.zip && \
    unzip *kotlin*.zip && \
    mv kotlinc /usr/share && \
    rm *kotlin*.zip

# install swift5 compiler
  RUN wget -qO- https://packagecloud.io/install/repositories/swift-arm/release/script.deb.sh | bash 
  RUN apt install -y swiftlang

# setup adb server
EXPOSE 5037

# set the environment variables
ARG JDK_VERSION=11
ENV JAVA_HOME /usr/lib/jvm/java-${JDK_VERSION}-openjdk-arm64
ENV GRADLE_HOME /usr/share/gradle
ENV KOTLIN_HOME /usr/share/kotlinc
ENV ANDROID_SDK_ROOT /usr/lib/android-sdk
ENV ANDROID_HOME /usr/lib/android-sdk
ENV ANDROID_TOOLS /usr/lib/android-sdk/tools
ENV ANDROID_PLATFORMS /usr/lib/android-sdk/platforms
ENV ANDROID_PLATFORM_TOOLS /usr/lib/android-sdk/platform-tools
ENV ANDROID_BUILD_TOOLS /usr/lib/android-sdk/build-tools
ENV ANDROID_CMDLINE_TOOLS /usr/lib/android-sdk/cmdline-tools/tools
ENV PATH ${PATH}:${GRADLE_HOME}/bin:${KOTLIN_HOME}/bin:${ANDROID_BUILD_TOOLS}/debian:${ANDROID_TOOLS}/bin:${ANDROID_PLATFORM_TOOLS}:${ANDROID_CMDLINE_TOOLS}/bin:${ANDROID_SDK_ROOT}/emulator

# install system image and accept licence, installation of emulator will fail for arm64 system
RUN yes Y | ${ANDROID_CMDLINE_TOOLS}/bin/sdkmanager --install "platform-tools" "system-images;android-24;google_apis;arm64-v8a" "platforms;android-24" "build-tools;30.0.3" && \
    wget https://github.com/qhuyduong/arm_adb/releases/download/v1.0.39-aarch64/adb && \
    chmod +x adb && mv adb /usr/lib/android-sdk/platform-tools/


# install and configure SSH server
EXPOSE 22
ADD sshd-banner /etc/ssh/
# ADD authorized_keys /tmp/
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends openssh-server supervisor locales && \
    mkdir -p /var/run/sshd /var/log/supervisord && \
    locale-gen en en_US en_US.UTF-8 && \
    apt-get remove -y locales && apt-get autoremove -y && \
    FILE_SSHD_CONFIG="/etc/ssh/sshd_config" && \
    echo "\nBanner /etc/ssh/sshd-banner" >> $FILE_SSHD_CONFIG && \
    echo "\nPermitUserEnvironment=yes" >> $FILE_SSHD_CONFIG && \
    ssh-keygen -q -N "" -f /root/.ssh/id_rsa && \
    FILE_SSH_ENV="/root/.ssh/environment" && \
    touch $FILE_SSH_ENV && chmod 600 $FILE_SSH_ENV && \
    printenv | grep "JAVA_HOME\|GRADLE_HOME\|KOTLIN_HOME\|ANDROID_SDK_ROOT\|LD_LIBRARY_PATH\|PATH" >> $FILE_SSH_ENV && \
    echo "\nauth required pam_env.so envfile=$FILE_SSH_ENV" >> /etc/pam.d/sshd && \
    FILE_AUTH_KEYS="/root/.ssh/authorized_keys" && \
    touch $FILE_AUTH_KEYS && chmod 600 $FILE_AUTH_KEYS && \
    for file in /tmp/*.pub; \
    do if [ -f "$file" ]; then echo "\n" >> $FILE_AUTH_KEYS && cat $file >> $FILE_AUTH_KEYS && echo "\n" >> $FILE_AUTH_KEYS; fi; \
    done && \
    (rm /tmp/*.pub 2> /dev/null || true)
  

# install and configure VNC server
ENV USER root
ENV DISPLAY :1
EXPOSE 5901
ADD vncpass.sh /tmp/
ADD watchdog.sh /usr/local/bin/
ADD supervisord_vncserver.conf /etc/supervisor/conf.d/
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends xfce4 xfce4-goodies xfonts-base dbus-x11 tightvncserver expect && \
    apt-get install -y --no-install-recommends firefox codeblocks && \
    chmod +x /tmp/vncpass.sh; sync && \
    /tmp/vncpass.sh && \
    rm /tmp/vncpass.sh && \
    apt-get remove -y expect && apt-get autoremove -y && \
    FILE_SSH_ENV="/root/.ssh/environment" && \
    echo "DISPLAY=:1" >> $FILE_SSH_ENV

# install and patch Visual Studio Code
RUN apt update && apt install -y libsecret-1-0 libxss1 libgbm1 && apt-get clean
RUN wget -O code_arm64.deb https://aka.ms/linux-arm64-deb && dpkg -i code_arm64.deb && rm code_arm64.deb
RUN sed 's/BIG-REQUESTS/_IG-REQUESTS/' /usr/lib/aarch64-linux-gnu/libxcb.so.1.1.0 | tee /usr/share/code/libxcb.so.1 > /dev/null


ADD supervisord.conf /etc/supervisor/conf.d/
CMD ["/usr/bin/supervisord"]